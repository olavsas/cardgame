package edu.ntnu.idatt2001.models;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Random;


/**
 * Represents a deck of cards.
 *
 * @author Olav Asprem
 */
public class DeckOfCards {

  private final Random rand = new Random();
  private final List<PlayingCard> cards = new ArrayList<>();
  private final char[] suits = {'H', 'D', 'C', 'S'};

  /**
   * Constructor for DeckOfCards.
   * <p>
   * Creates 52 {@link PlayingCard}-objects and stores them
   * </p>
   */
  public DeckOfCards() {
    for (char suit : suits) {
      for (int i = 0; i < 13; i++) {
        int face = i + 1;
        PlayingCard card = new PlayingCard(suit, face);
        cards.add(card);
      }
    }
  }

  /**
   * Returns a list of randomly selected playing cards.
   * <p>
   * The cards are removed from the deck as they are dealt. Returns null if the deck becomes
   * depleted.
   * </p>
   *
   * @param n the number of cards to deal
   * @return the list of cards, or null if deck is depleted
   */
  public List<PlayingCard> dealHand(int n) {
    if (n > cards.size()) {
      return Collections.emptyList();
    }
    List<PlayingCard> cardHand = new ArrayList<>();

    for (int i = 0; i < n; i++) {
      int randCardIndex = rand.nextInt(cards.size());
      PlayingCard randomCard = cards.remove(randCardIndex);
      cardHand.add(randomCard);
    }
    return cardHand;
  }

  /**
   * Resets the deck.
   */
  public void resetDeck() {
    cards.clear();
    for (char suit : suits) {
      for (int i = 0; i < 13; i++) {
        int face = i + 1;
        PlayingCard card = new PlayingCard(suit, face);
        cards.add(card);
      }
    }
  }
}
