package edu.ntnu.idatt2001.models;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

/**
 * Represents a hand of cards.
 *
 * @author Olav Asprem
 */
public class CardHand {

  private final List<PlayingCard> hand = new ArrayList<>();

  /**
   * Instantiates a new Card hand.
   *
   * @param cards the cards
   */
  public CardHand(PlayingCard... cards) {
    if(cards == null || cards.length == 0) {
      throw new IllegalArgumentException("cards can't be null or empty");
    }

    Collections.addAll(this.hand, cards);
  }

  /**
   * Returns the sum of the card faces on this hand.
   *
   * @return the sum of the card faces, as an integer
   */
  public int getSumOfCardFaces() {
    return this.hand.stream()
        .mapToInt(PlayingCard::getFace)
        .sum();
  }

  /**
   * Returns an array with String representations of the cards of hearts in this hand.
   *
   * @return String representations of the cards of hearts in this hand.
   */
  public String[] getCardsOfHearts() {
    List<String> cardsOfHeartsList = this.hand.stream()
        .filter(card -> card.getSuit() == 'H')
        .map(PlayingCard::getAsString)
        .toList();

    return cardsOfHeartsList.toArray(new String[0]);
  }

  /**
   * Checks if the queen of spades is contained in the hand.
   *
   * @return returns true if the queen of spades is in the hand, false otherwise.
   */
  public boolean containsQueenOfSpades() {
    return hand.stream().anyMatch(o -> o.getAsString().equals("S12"));
  }

  /**
   * Checks whether the hand is a flush or not. A flush is a hand where every suit is the same.
   *
   * @return true if the hand is a flush, false otherwise
   */
  public boolean isFlush() {
    return hand.stream().map(PlayingCard::getSuit)
        .allMatch(o -> o == hand.get(0).getSuit());
  }
}
