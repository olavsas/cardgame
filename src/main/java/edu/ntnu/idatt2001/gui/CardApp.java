package edu.ntnu.idatt2001.gui;

import java.util.Arrays;
import javafx.application.Application;
import javafx.geometry.Insets;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.TextArea;
import javafx.scene.control.TextField;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import javafx.stage.Stage;

/**
 * Application class for the card game.
 * <p>
 * Creates the GUI elements and connects to the {@link CardGameController}.
 * </p>
 *
 * @author Olav Asprem
 */
public class CardApp extends Application {

  /**
   * Text field that displays the sum of the faces of the current hand.
   */
  TextField sumOfFacesDisplay;
  /**
   * Text field that list the cards of hearts in the current hand.
   */
  TextField cardsOfHeartsDisplay;
  /**
   * Text field that says yes/no for whether the current hand is a flush.
   */
  TextField flushDisplay;
  /**
   * Text field that says yes/no for whether the current hand contains the queen of spades.
   */
  TextField queenOfSpadesDisplay;
  /**
   * The text area that displays the cards in the current hand.
   */
  private TextArea cardHandDisplayText;
  /**
   * Controller for connecting the GUI to the business-logic.
   */
  private CardGameController gameController;

  /**
   * The entry point of application.
   *
   * @param args the input arguments
   */
  public static void main(String[] args) {
    launch(args);
  }

  /**
   * Initializes the application.
   * <p>
   * Sets up the GUI and creates a controller.
   * </p>
   *
   * @param primaryStage the primary stage of this application
   */
  @Override
  public void start(Stage primaryStage) {
    this.gameController = new CardGameController(this);

    BorderPane root = new BorderPane();
    Scene scene = new Scene(root, 600, 400);
    primaryStage.setTitle("Card Game");
    primaryStage.setScene(scene);
    primaryStage.show();

    VBox leftContainer = new VBox();
    leftContainer.setPrefWidth(root.getWidth() * 0.7);
    leftContainer.setPadding(new Insets(30, 0, 30, 10));
    leftContainer.setSpacing(20);

    VBox rightContainer = new VBox();
    rightContainer.setPrefWidth(root.getWidth() * 0.3);
    rightContainer.setPadding(new Insets(30, 0, 30, 10));
    rightContainer.setSpacing(20);

    root.setLeft(leftContainer);
    root.setRight(rightContainer);

    Button btnDealHand = new Button("Deal hand");
    btnDealHand.setOnAction(event -> this.gameController.handleBtnDealHand());
    Button btnCheckHand = new Button("Check hand");
    btnCheckHand.setOnAction(event -> this.gameController.handleBtnCheckHand());
    Button btnResetDeck = new Button("Reset deck");
    btnResetDeck.setOnAction(event -> this.gameController.handleBtnResetDeck());

    rightContainer.getChildren().add(btnDealHand);
    rightContainer.getChildren().add(btnCheckHand);
    rightContainer.getChildren().add(btnResetDeck);

    cardHandDisplayText = new TextArea();
    leftContainer.getChildren().add(cardHandDisplayText);

    HBox infoGroup1 = new HBox();
    infoGroup1.setSpacing(10);
    leftContainer.getChildren().add(infoGroup1);

    // Creat display field for sum of faces
    sumOfFacesDisplay = new TextField();
    sumOfFacesDisplay.setPrefWidth(30);
    Label sumOfFacesLabel = new Label("Sum of faces");
    sumOfFacesLabel.setLabelFor(sumOfFacesDisplay);
    infoGroup1.getChildren().add(sumOfFacesLabel);
    infoGroup1.getChildren().add(sumOfFacesDisplay);

    // create display field for showing cards of hearts
    cardsOfHeartsDisplay = new TextField();
    Label cardsOfHeartsLabel = new Label("Cards of hearts");
    cardsOfHeartsLabel.setLabelFor(cardsOfHeartsDisplay);
    infoGroup1.getChildren().add(cardsOfHeartsLabel);
    infoGroup1.getChildren().add(cardsOfHeartsDisplay);

    HBox infoGroup2 = new HBox();
    infoGroup2.setSpacing(10);
    leftContainer.getChildren().add(infoGroup2);

    flushDisplay = new TextField();
    flushDisplay.setPrefWidth(50);
    Label flushDisplayLabel = new Label("Flush: ");
    flushDisplayLabel.setLabelFor(flushDisplay);
    infoGroup2.getChildren().add(flushDisplayLabel);
    infoGroup2.getChildren().add(flushDisplay);

    queenOfSpadesDisplay = new TextField();
    queenOfSpadesDisplay.setPrefWidth(50);
    Label queenOfSpadesDisplayLabel = new Label("Queen of spades: ");
    queenOfSpadesDisplayLabel.setLabelFor(queenOfSpadesDisplay);
    infoGroup2.getChildren().add(queenOfSpadesDisplayLabel);
    infoGroup2.getChildren().add(queenOfSpadesDisplay);

  }

  /**
   * Display card hand text.
   *
   * @param outText the out text
   */
  public void displayCardHandText(String outText) {
    this.cardHandDisplayText.setText(outText);
  }

  /**
   * Display check hand info.
   *
   * @param sumOfFaces    the sum of faces
   * @param cardsOfHearts the cards of hearts
   * @param flush         the flush
   * @param queenOfSpades the queen of spades
   */
  public void displayCheckHandInfo(int sumOfFaces, String[] cardsOfHearts,
      boolean flush, boolean queenOfSpades) {

    this.sumOfFacesDisplay.setText(Integer.toString(sumOfFaces));

    String cardsOfHeartsOutString;
    if (cardsOfHearts.length == 0) {
      cardsOfHeartsOutString = "No cards of hearts.";
    } else {
      cardsOfHeartsOutString = Arrays.stream(cardsOfHearts)
          .reduce((s1, s2) -> s1.concat(" " + s2))
          .orElse("No cards of hearts");
    }
    this.cardsOfHeartsDisplay.setText(cardsOfHeartsOutString);

    String queenOfSpadesOutString = (queenOfSpades) ? "Yes" : "No";
    this.queenOfSpadesDisplay.setText(queenOfSpadesOutString);

    String flushOutString = (flush) ? "Yes" : "No";
    this.flushDisplay.setText(flushOutString);
  }
}
