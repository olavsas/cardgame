package edu.ntnu.idatt2001.gui;

import edu.ntnu.idatt2001.models.CardHand;
import edu.ntnu.idatt2001.models.DeckOfCards;
import edu.ntnu.idatt2001.models.PlayingCard;
import java.util.List;


/**
 * Controller class for the Card Game.
 * <p>
 * Connects the GUI and business-logic.
 * </p>
 *
 * @author Olav Asprem
 */
public class CardGameController {

  // The number of cards dealt for each hand
  private static final int NUM_CARDS_PER_HAND = 5;
  private final CardApp cardApp;
  private final DeckOfCards deck;
  private CardHand hand;

  /**
   * Instantiates a new Card game controller.
   *
   * @param cardApp the card app
   */
  public CardGameController(CardApp cardApp) {
    this.cardApp = cardApp;
    this.deck = new DeckOfCards();
  }

  /**
   * Creates a new card hand and displays the cards in the GUI.
   */
  public void handleBtnDealHand() {
    List<PlayingCard> cardHandList = deck.dealHand(NUM_CARDS_PER_HAND);
    String displayText;
    if (cardHandList.isEmpty()) {
      displayText = "Out of cards, please reset the deck.";
      this.hand = null;
    } else {
      this.hand = new CardHand(cardHandList.toArray(new PlayingCard[0]));

      displayText = cardHandList.stream()
          .map(PlayingCard::getAsString)
          .reduce((o1, o2) -> o1.concat("  " + o2))
          .orElse("Out of cards");
    }
    cardApp.displayCardHandText(displayText);
  }

  /**
   * Handle btn check hand.
   */
  public void handleBtnCheckHand() {
    if (hand == null) {
      cardApp.displayCheckHandInfo(0, new String[0], false, false);
    } else {
      int sumOfFaces = this.hand.getSumOfCardFaces();
      String[] cardsOfHearts = hand.getCardsOfHearts();
      boolean queenOfSpades = hand.containsQueenOfSpades();
      boolean isFlush = hand.isFlush();
      cardApp.displayCheckHandInfo(sumOfFaces, cardsOfHearts, isFlush, queenOfSpades);
    }
  }

  public void handleBtnResetDeck() {
    this.deck.resetDeck();
    cardApp.displayCardHandText("Deck has been reset.");
  }
}
