package edu.ntnu.idatt2001.gui;

/**
 * Launcher for the card game application.
 */
public class CardAppLauncher {

  /**
   * The entry point of application.
   *
   * @param args the input arguments
   */
  public static void main(String[] args) {
    CardApp.main(args);
  }
}
