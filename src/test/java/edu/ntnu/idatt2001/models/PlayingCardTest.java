package edu.ntnu.idatt2001.models;

import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

/**
 * Test for the class {@link PlayingCard}.
 */
class PlayingCardTest {
  PlayingCard cardH12;
  PlayingCard cardD1;

  @BeforeEach
  void setUp() {
    cardH12 = new PlayingCard('H', 12);
    cardD1 = new PlayingCard('D', 1);
  }

  @Test
  void constructorWorksWithValidArgs() {
    assertDoesNotThrow(() -> new PlayingCard('D', 10));
    assertDoesNotThrow(() -> new PlayingCard('H', 1));
    assertDoesNotThrow(() -> new PlayingCard('C', 13));
    assertDoesNotThrow(() -> new PlayingCard('S', 5));
  }

  @Test
  void constructorThrowsWithInvalidArgs() {
    assertThrows(IllegalArgumentException.class, () -> {
      new PlayingCard('J', 10);
    });
    assertThrows(IllegalArgumentException.class, () -> {
      new PlayingCard('s', 10);
    });
    assertThrows(IllegalArgumentException.class, () -> {
      new PlayingCard('S', 14);
    });
    assertThrows(IllegalArgumentException.class, () -> {
      new PlayingCard('S', 0);
    });
  }

  @Test
  void getAsString() {
    assertEquals("H12", cardH12.getAsString());
    assertEquals("D1", cardD1.getAsString());
  }

  @Test
  void getSuit() {
    assertEquals('H', cardH12.getSuit());
    assertEquals('D', cardD1.getSuit());
  }

  @Test
  void getFace() {
    assertEquals(12, cardH12.getFace());
    assertEquals(1, cardD1.getFace());
  }
}