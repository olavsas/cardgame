package edu.ntnu.idatt2001.models;

import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

class CardHandTest {

  private CardHand handNotFlushSum15;
  private CardHand handIsFlushSum6;
  private CardHand handContainsQueenOfSpades;


  @BeforeEach
  void setUp() {
    PlayingCard cardH12 = new PlayingCard('H', 12);
    PlayingCard cardD1 = new PlayingCard('D', 1);
    PlayingCard cardD2 = new PlayingCard('D', 2);
    PlayingCard cardD3 = new PlayingCard('D', 3);
    PlayingCard cardS12 = new PlayingCard('S', 12);

    handNotFlushSum15 = new CardHand(cardH12, cardD1, cardD2);
    handIsFlushSum6 = new CardHand(cardD1, cardD2, cardD3);
    handContainsQueenOfSpades = new CardHand(cardS12, cardD1, cardD2);
  }

  @Test
  void constructorThrowsWithInvalidArgs() {
    assertThrows(IllegalArgumentException.class, () -> {
      new CardHand(null);
    });
    PlayingCard[] emptyCardArr = {};
    assertThrows(IllegalArgumentException.class, () -> {
      new CardHand(emptyCardArr);
    });
  }
  @Test
  void getSumOfCardFaces() {
    assertEquals(6, handIsFlushSum6.getSumOfCardFaces());
    assertEquals(15, handNotFlushSum15.getSumOfCardFaces());
  }

  @Test
  void getCardsOfHearts() {
    assertArrayEquals(new String[]{"H12"}, handNotFlushSum15.getCardsOfHearts());
    assertArrayEquals(new String[]{}, handIsFlushSum6.getCardsOfHearts());
  }

  @Test
  void containsQueenOfSpades() {
    assertFalse(handIsFlushSum6.containsQueenOfSpades());
    assertFalse(handNotFlushSum15.containsQueenOfSpades());
    assertTrue(handContainsQueenOfSpades.containsQueenOfSpades());
  }

  @Test
  void isFlush() {
    assertTrue(handIsFlushSum6.isFlush());
    assertFalse(handNotFlushSum15.isFlush());
  }
}