package edu.ntnu.idatt2001.models;

import static org.junit.jupiter.api.Assertions.*;

import java.util.List;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

class DeckOfCardsTest {

  DeckOfCards deck;

  @BeforeEach
  void setUp() {
    deck = new DeckOfCards();
  }

  @Test
  void dealHand() {
    List<PlayingCard> hand = deck.dealHand(5);
    assertEquals(5, hand.size());
  }
}